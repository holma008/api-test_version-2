﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignatureStorageAPI.Models
{
    public class Client
    {
        public int ID { get; set; }
        public string ClientName { get; set; }
        public string TermsConditions { get; set; }
        public byte[] Signature { get; set; }
        public string Urls { get; set; }
    }
}