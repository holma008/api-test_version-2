﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using SignatureStorageAPI.Models;

namespace SignatureStorageAPI
{
    public class ClientPersistence
    {
        public bool UpdateClient(int id, Client client)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                SqlDataReader myReader = null;

                string query = "SELECT * FROM Clients WHERE ID = " + id.ToString();
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    myReader.Close();

                    //if (client.Signature == null)//temporary method until client side is complete
                    //{
                    //    Bitmap sig = new Bitmap("C:\\Users\\bholm\\OneDrive\\Documents\\CS 481\\HW3_1.png");//retrieves local image as bitmap
                    //    var ms = new MemoryStream();//create memory stream, this is where the bytes are deposited
                    //    sig.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);//save bytes to memory stream variable ms
                    //    client.Signature = ms.ToArray();
                    //}

                    //query = "UPDATE Clients SET ClientName = '" + client.ClientName + "', TermsConditions = '" + client.TermsConditions + "', Signature = @Signature WHERE  ID = " + id.ToString();//, Signature = '" + user.Signature + "' WHERE UserID = " + id.ToString();
                    query = "UPDATE Clients SET Signature = @Signature WHERE  ID = " + id.ToString();//, Signature = '" + user.Signature + "' WHERE UserID = " + id.ToString();
                    cmd = new SqlCommand(query, connection);
                    cmd.Parameters.Add(new SqlParameter("@Signature", client.Signature));//why like this though?, why not just user.Signature in the query line?

                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public string GetTermsConditions(int id)
        {
            SqlConnection connection;
            string connectionString = ConfigurationManager.ConnectionStrings["Studio2BDataBase"].ConnectionString;
            connection = new SqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                Client client = new Client();
                SqlDataReader myReader = null;

                string query = "SELECT * FROM Clients WHERE ID = " + id.ToString();
                SqlCommand cmd = new SqlCommand(query, connection);

                myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    string TextBlock = myReader.GetString(2);
                    return TextBlock;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}