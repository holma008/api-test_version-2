﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SignatureStorageAPI.Models;
using System.Web.Http.Cors;

namespace SignatureStorageAPI.Controllers
{
    //[EnableCors(origins: "*", "*", "*")]
    public class SignatureController : ApiController
    {
        /*
        // GET: api/Signature
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        */
        // GET: api/Signature/5
        public string Get(int id)
        {
            ClientPersistence cp = new ClientPersistence();
            string TextBlock = cp.GetTermsConditions(id);
            if (TextBlock == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return TextBlock;
        }
        /*
        // POST: api/Signature
        public void Post([FromBody]string value)
        {
        }
        */

        // PUT: api/Signature/5
        public HttpResponseMessage Put(int id, [FromBody]Client value)
        {
            ClientPersistence cp = new ClientPersistence();
            bool Existed = false;
            Existed = cp.UpdateClient(id, value);

            HttpResponseMessage response;

            if (Existed)
            {
                response = Request.CreateResponse(HttpStatusCode.Found);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
        /*
        // DELETE: api/Signature/5
        public void Delete(int id)
        {
        }
        */
    }
}
