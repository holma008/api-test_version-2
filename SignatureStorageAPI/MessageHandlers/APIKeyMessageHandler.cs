﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SignatureStorageAPI.MessageHandlers
{
    public class APIKeyMessageHandler : DelegatingHandler
    {
        private const string APIKeyToCheck = "Studio2B!";

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage RequestMessage, CancellationToken Token)
        {
            bool ValidKey = false;
            IEnumerable<string> RequestHeaders;
            var CheckAPIKeyExists = RequestMessage.Headers.TryGetValues("APIKey", out RequestHeaders);
            if (CheckAPIKeyExists)
            {
                if (RequestHeaders.FirstOrDefault().Equals(APIKeyToCheck))
                {
                    ValidKey = true;
                }
            }
            if (!ValidKey)
            {
                return RequestMessage.CreateResponse(HttpStatusCode.Forbidden, "Invalid API Key");
            }

            var Response = await base.SendAsync(RequestMessage, Token);
            return Response;
        }
    }
}